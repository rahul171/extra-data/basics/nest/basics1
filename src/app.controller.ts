import {
  Body,
  Controller,
  Get,
  HttpCode,
  Next,
  Param,
  Headers,
  Ip,
  Query,
  Req,
  Res,
  Post,
  Header,
  Redirect,
  HttpStatus,
} from '@nestjs/common';
import { AppService } from './app.service';
import { Request } from 'express';
import { AppGet1Dto, AppGet2Dto, AppPostDto } from './dto';

@Controller('home')
export class AppController {
  constructor(private appService: AppService) {}

  @Get()
  @HttpCode(400)
  home(): string {
    return this.appService.greet();
  }

  @Get('/data1')
  data1() {
    return {
      msg: 'data1',
    };
  }

  @Get('/data2')
  data2(@Res() res) {
    res.status(300).send('data2');
  }

  @Get('/data3')
  @HttpCode(200)
  data3(@Req() req, @Res() res, @Next() next) {
    console.log('in handler data3 =>', req.url);
    // res.status(400).send('data3');
    next();
  }

  @Get('/data3')
  data3_2(@Req() req: Request) {
    console.log('in handler data3_2 =>', req.url);
    return 'data3_2';
  }

  @Post('/data4/:id')
  data4(
    @Param() params,
    @Query() query,
    @Body() body,
    @Headers() headers,
    @Ip() ip,
  ) {
    const data = { params, query, body, headers, ip };
    console.log(data);
    return data;
  }

  @Get('data5/start.-(abc)*-end')
  data5() {
    return 'data5';
  }

  // * matched everything, can't use (abc)*, because it equivalent to abc(.*)
  // to get the result of (abc)* as expected, use ((abc)?)+
  @Get('data6/start-((abc)?)+-end')
  data6() {
    return 'data6';
  }

  @Get('data7')
  data7() {
    throw new Error('an error');
    return 'data7';
  }

  @Get('data8')
  @Header('X-Powered-By1', 'value 1')
  @Header('X-Powered-By2', 'value 1')
  data8() {
    return 'data8';
  }

  @Get('data9')
  // If no status code (2nd argument) is provided, 302 is used by default.
  @Redirect('https://www.google.com')
  data9() {
    if (Math.random() < 0.5) {
      return {
        url: 'http://www.google.com/?q=0.5',
        // Omitting the status code here will use the status code of @Redirect() decorator.
        statusCode: 301,
      };
    }
  }

  @Get('data10')
  async data10(): Promise<string> {
    return await ((): Promise<string> => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve('hello');
        }, 2000);
      });
    })();
    // or return the promise directly.
  }

  @Get('data11')
  // http code doesn't matter here. it will return 500: internal server error
  // when exception occurs.
  @HttpCode(HttpStatus.CREATED)
  data11() {
    throw new Error('halo');
    return 'data11';
  }

  @Post('data12/:id1/:id2/hola')
  data12(
    @Param('id1') id1: AppGet1Dto,
    @Param('id2') id2: AppGet2Dto,
    @Body() body: AppPostDto,
  ) {
    const data = { id1, id2, body };
    console.log(data);
    return data;
  }
}
