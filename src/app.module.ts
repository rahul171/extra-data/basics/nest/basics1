import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AbcdService } from './abcd/abcd.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, AbcdService],
})
export class AppModule {}
