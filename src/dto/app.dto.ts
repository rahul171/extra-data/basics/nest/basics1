export class AppGet1Dto {
  id1: number;
}

export class AppGet2Dto {
  id2: string;
}

export class AppPostDto {
  name: string;
  a: number;
  b: boolean;
}
