import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  // const app = await NestFactory.create(AppModule);
  // app.set('view engine', 'hbs');
  app.use((req, res, next) => {
    console.log(req.method, req.url);
    next();
  });
  await app.listen(3000);
}

bootstrap();
