import { Test, TestingModule } from '@nestjs/testing';
import { LoggerService } from './logger.service';

describe('logger service test', () => {
  let service: LoggerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LoggerService],
    }).compile();

    service = module.get<LoggerService>(LoggerService);
  });

  it('first it', async () => {
    expect(service).toBeDefined();
    expect(service.log).toBeDefined();
    expect(service.generateRandomNumber(1, 5)).toBeGreaterThanOrEqual(1);
    expect(service.generateRandomNumber(1, 5)).toBeLessThanOrEqual(5);
  });

  it('multiple instances', () => {
    expect(service.getValues()).toEqual([]);
    service.addValue(11);
    expect(service.getValues().length).toBe(1);
    service.addValue(22);
    expect(service.getValues().length).toBe(2);
  });

  it('multiple instances 2', () => {
    expect(service.getValues()).toEqual([]);
  });
});
