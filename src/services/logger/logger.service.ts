import { Injectable } from '@nestjs/common';

@Injectable()
export class LoggerService {
  private values: number[] = [];

  log(message: any = '\n'): void {
    console.log(message);
  }

  /**
   * Generated random number between two given numbers. [Inclusive]
   *
   * @param a number Start number.
   * @param b number End number.
   *
   * @return number A random number.
   */
  generateRandomNumber(a: number, b: number): number {
    return a + Math.floor((b - a + 1) * Math.random());
  }

  addValue(value: number): number {
    this.values.push(value);
    return value;
  }

  getValues(): number[] {
    return this.values;
  }
}
