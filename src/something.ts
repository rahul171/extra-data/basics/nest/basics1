class Abcd {
  private a: string;
  private b: string = '';
  private c: string = 'hello';

  private static a1 = 'aa';
  static b1 = 'aa';
  // this is not static. its bound to the instance.
  c1 = 'aa';

  constructor(private aa, private bb) {}

  hello() {
    console.log('there');
  }
}
