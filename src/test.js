let dd;

async function foo() {
  console.log('start 1');
  const p1 = new Promise(resolve => {
    console.log('promise 1');
    setTimeout(() => resolve('1'), 3000);
  });
  console.log('start 2');
  const p2 = new Promise((_, reject) => {
    console.log('promise 2');
    setTimeout(() => reject('2'), 500);
  });
  console.log('start 3');
  dd = Date.now();
  const results = [await p1, await p2]; // Do not do this! Use Promise.all or Promise.allSettled instead.
  console.log('start 4');
}
foo().catch(err => {
  console.log('== catch it ==');
  console.log('err:', err);
  console.log((Date.now() - dd) / 1000);
}); // Attempt to swallow all errors...
